// Requires
const path = require('path');
const express = require('express')
const expressHandlebars = require('express-handlebars');
const app = express();

// Application Settings
app.set('port', (process.env.PORT || 5000));
app.engine( 'hbs', expressHandlebars( {
  extname: 'hbs',
  defaultLayout: 'main',
  layoutsDir: __dirname + '/views/layouts/',
  partialsDir: __dirname + '/views/partials/'
} ) );
app.set( 'view engine', 'hbs' );

// Static Assets
app.use('/public', express.static(path.join(__dirname, 'public')))
app.use('/favicon.ico', express.static(path.join(__dirname, 'favicon.ico')))

// Main View
app.get('/', function (req, res) {
  res.render(__dirname + '/views/index.hbs');
})

app.listen(app.get('port'), function () {
  console.log('App started on port: ' + app.get('port'));
})
