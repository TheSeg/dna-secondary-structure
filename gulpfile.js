'use strict';

const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const gulpDir = path.join(__dirname, 'gulp');
const options = require(path.join(__dirname, '/config'));

// Load Gulp tasks.
fs.readdirSync(gulpDir).forEach(function (file) {
  if (path.extname(file) === '.js') {
    require(path.join(gulpDir, file))(gulp, options);
  }
})
