'use strict';

const path = require('path');
const argv = require('yargs').argv;

module.exports = {
  pkg: require(path.join(__dirname, 'package.json')),

  isDev: argv.d || argv.dev || argv.isdev || false,
  isWatch: argv.watch || argv.iswatch || false,

  rootDir: __dirname
}
