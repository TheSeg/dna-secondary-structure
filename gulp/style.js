'use strict';

const path = require('path');
const gulpif = require('gulp-if');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const stylelint = require('gulp-stylelint');
const sourcemaps = require('gulp-sourcemaps');
const livereload = require('gulp-livereload');

module.exports = function (gulp, options) {

  let mainScss = path.join(options.rootDir, 'src/style/app.scss');

  gulp.task('style:lint', function () {
    return gulp.src(mainScss)
      .pipe(stylelint({
        failAfterError: false,
        reporters: [
          {
            formatter: 'string',
            console: true
          }
        ]
      }))
  });

  gulp.task('style:build', function () {
    return gulp.src(mainScss)
      .pipe(gulpif(options.isDev, sourcemaps.init()))
      .pipe(sass({
        outputStyle: 'compressed',
        includePaths: require('node-normalize-scss').includePaths
      }).on('error', sass.logError))
      .pipe(postcss([
        autoprefixer()
      ]))
      .pipe(gulpif(options.isDev, sourcemaps.write('./')))
      .pipe(gulp.dest('./public'))
      .pipe(gulpif(options.isDev, livereload()));
  });

  gulp.task('style', ['style:lint', 'style:build']);

}
