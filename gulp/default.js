'use strict';

module.exports = function (gulp, options) {
  let buildTasks = [
    'clean',
    'style',
    'scripts',
  ];

  gulp.task('main', buildTasks, function(){
    if (options.isWatch) {
      gulp.start('watch');
    }
  });

  gulp.task('default', ['main']);
}
