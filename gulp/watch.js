'use strict';

const gutil = require('gulp-util');
const livereload = require('gulp-livereload');

module.exports = function (gulp) {

  gulp.task('watch', function () {
    gutil.log('ℹ️  Watch / LiveReload:', gutil.colors.bold.green('ENABLED'));

    livereload.listen({
      start: true
    });

    // Scripts
    gulp.watch('src/scripts/**/*.js', ['scripts']);

    // Style
    gulp.watch('src/style/**/*.scss', ['style']);

    // HTML / Handlebars
    gulp.watch('views/**/*.{html,hbs}', function(){
      livereload.reload();
    });

  });

}
