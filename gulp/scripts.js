'use strict';

const path = require('path');
const gutil = require('gulp-util');
const gulpif = require('gulp-if');
const eslint = require('gulp-eslint');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const livereload = require('gulp-livereload');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

module.exports = function (gulp, options) {

  gulp.task('scripts:lint', function() {
    if (!options.isDev) {
      return true;
    } else {
      return gulp.src(path.join(options.rootDir, 'src/scripts/**/*.js'), {
          base: './src/scripts'
        })
        .pipe(eslint(path.join(options.rootDir, '.eslintrc.json')))
        .pipe(eslint.format());
    }
  })

  gulp.task('scripts:build', ['scripts:lint'], function() {

    var bundler = browserify('src/scripts/app.js', {
      debug: true,
      entries: ['./src/scripts/app.js'],
      cache: {},
      packageCache: {},
      transform: [
        [
          'babelify',
          {
            'presets': ['es2015'],
            'sourceMaps': options.isDev ? true : false
          }
        ]
      ]
    });

    return bundler.bundle()
      .pipe(source('app.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(uglify().on('error', gutil.log))
      .pipe(gulpif(options.isDev, sourcemaps.write('./')))
      .pipe(gulp.dest(path.join(options.rootDir, 'public/')))
      .pipe(gulpif(options.isDev, livereload()))
      .on('error', gutil.log);
  });

  gulp.task('scripts', ['scripts:build']);

}
