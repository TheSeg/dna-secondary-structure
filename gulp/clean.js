'use strict';

const path = require('path');
const gutil = require('gulp-util');
const del = require('del');

module.exports = function (gulp, options) {

  gulp.task('clean:public', function() {
    let publicFolder = path.join(options.rootDir, 'public/**/*');
    gutil.log('Cleaning: ', publicFolder);
    return del(publicFolder);
  });

  gulp.task('clean', ['clean:public']);

}
