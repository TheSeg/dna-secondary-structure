import {default as state} from '../../modules/State';
import {default as Controls} from '../../modules/Controls';
import {default as MainStage} from '../../modules/MainStage';
import {default as Sequencer} from '../../modules/Sequencer';

state.getData().then(() => {
  let controls = new Controls();
  let mainStage = new MainStage();
  let sequencer = new Sequencer();

  setTimeout(function() {
    mainStage.render();
    sequencer.build();
    mainStage.buildVisualization()
    controls.render();
  },10);
})
