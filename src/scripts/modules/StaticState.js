
class StaticState {
  constructor(options) {
    console.info('StaticState::constructor', options);
    this._constructor = Object.getPrototypeOf(this).constructor;
    this._constructor.STATE = this._constructor.STATE || {};
    this._constructor.HANDLERS = this._constructor.HANDLERS || {};
  }

  defineProperty(property) {
    var proto = Object.getPrototypeOf(this);

    if (Object.prototype.hasOwnProperty.call(proto, property)) {
      return;
    }

    Object.defineProperty(proto, property, {
      get: () => {
        return this._constructor.STATE[property];
      },
      set: (value) => {
        let handlers = this._constructor.HANDLERS[property] || [];
        let old = this._constructor.STATE[property];
        this._constructor.STATE[property] = value;

        // Are we being watched? Let's call them.
        for (let i = 0; i < handlers.length; i++) {
          if (old !== value) {
            handlers[i].call(this, value, property, old);
          }
        }
      }
    });
  }

  set(property, value) {
    // If we're setting an object, casscade the value down.
    if (typeof property === 'object') {
      let properties = property;
      let keys = Object.keys(properties);
      for (let i = 0; i < keys.length; i++) {
        let property = keys[i];
        this.defineProperty(property);
        this[property] = properties[property];
      }
      return;
    }

    this.defineProperty(property)
    this[property] = value;
  }

  get(property) {
    if (property) {
      // Return the property requested.
      return this[property];
    } else {
      // Return the whole thing.
      return this._constructor.STATE;
    }
  }

  watch(property, callback) {
    if (Array.isArray(property)) {
      for (let i = 0; i < property.length; i++) {
        this.watch(property[i], callback);
      }
      return;
    }

    this._constructor.HANDLERS[property] = this._constructor.HANDLERS[property] || [];
    this._constructor.HANDLERS[property].push(callback);
  }

  unwatch(property, callback) {
    if (Array.isArray(property)) {
      for (let i = 0; i < property.length; i++) {
        this.unwatch(property[i], callback);
      }
      return;
    }

    this._constructor.HANDLERS[property] = this._constructor.HANDLERS[property] || [];

    for (let i = 0; i < this._constructor.HANDLERS[property].length; i++) {
      if (this._constructor.HANDLERS[property][i] === callback) {
        this._constructor.HANDLERS[property].splice(i, 1);
      }
    }
  }

}

export default StaticState;
