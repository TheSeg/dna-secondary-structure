
class Utilities {

  constructor(options) {
    console.info('Utilities::constructor', options);
  }

  countOccurances(inputArray, needle) {
    if (typeof inputArray === 'string') {
      inputArray = inputArray.split('');
    }

    return inputArray.reduce(function(n, val) {
      return n + (val === needle);
    }, 0);
  }

  countOccurancesNot(inputArray, needleArray) {
    if (typeof inputArray === 'string') {
      inputArray = inputArray.split('');
    }

    if (typeof needleArray === 'string') {
      needleArray = needleArray.split('');
    }

    let numOfInvalid = inputArray.reduce(function(n, val) {
      for (let a = 0; a < needleArray.length; a++) {
        n = n + (val === needleArray[a]);
      }
      return n;
    }, 0);

    if (numOfInvalid !== inputArray.length) {
      return false;
    } else {
      return true;
    }
  }

  getContrastYIQ(hexcolor) {
    if (hexcolor.includes('#')) {
      hexcolor = hexcolor.replace('#', '');
    }
    let r = parseInt(hexcolor.substr(0,2),16);
    let g = parseInt(hexcolor.substr(2,2),16);
    let b = parseInt(hexcolor.substr(4,2),16);
    let yiq = ((r*299)+(g*587)+(b*114))/1000;

    return (yiq >= 128) ? 'black' : 'white';
  }
}

export default new Utilities();
