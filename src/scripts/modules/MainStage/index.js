import {default as state} from '../State';
import {default as StaticState} from '../StaticState';
import {default as Nodes} from '../Nodes';
import {default as Sequencer} from '../../modules/Sequencer';

const DEFAULTS = {

}

class MainStage extends StaticState {

  constructor(options) {
    super();
    console.info('MainStage::Constructor', options);

    this.nodes = new Nodes();
    this.sequencer = new Sequencer();
  }

  render() {
    let mainStage = d3.select('#mainStageRender')
      .append('svg')
      .attr('width', '100%')
      .attr('height', '100%');

    state.set('mainStage', mainStage);
  }

  buildVisualization() {
    let allNodes = state.get('pairedSequence');

    for (let a = 0; a < allNodes.length; a++) {
      this.nodes.createNode(allNodes[a]);
    }

    this.sequencer.setPositions();
  }

}

MainStage.DEFAULTS = DEFAULTS;
export default MainStage;
