import {default as StaticState} from '../StaticState';

class State extends StaticState {

  constructor(options) {
    super();
    console.log('State::constructor', options);
  }

  getData() {
    this.dataPromise = this.dataPromise || new Promise((resolve) => {
      let promises = [];

      Promise.all(promises).then(() => {
        resolve(this);
      });
    });

    return this.dataPromise;
  }

}

export default new State();
