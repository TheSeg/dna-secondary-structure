import {default as state} from '../State';
import {default as StaticState} from '../StaticState';

const DEFAULTS = {
  defaultColors: {
    'C': '#00ff00',
    'G': '#ffff00',
    'A': '#00ffff',
    'T': '#ff0000',
    'N': '#cccccc',
  },
  defualtNodeSize: 5
}

class Controls extends StaticState {

  constructor(options) {
    super();
    console.info('Controls::Constructor', options, DEFAULTS);

    // Set defaults and watch for node color options.
    for (const key of Object.keys(DEFAULTS.defaultColors)) {
      let stateName = 'options-node-color-' + key.toLowerCase();
      state.set(stateName, DEFAULTS.defaultColors[key]);
    }
    // Set defaults and watch for node size.
    state.set('options-node-size', DEFAULTS.defualtNodeSize);
  }

  render() {
    // Node Color Listeners
    for (const key of Object.keys(DEFAULTS.defaultColors)) {
      document.getElementById('input-colors-' + key.toLowerCase()).addEventListener('change', function() {
        let targetNodeType = this.id.replace('input-colors-','');
        state.set('options-node-color-' + targetNodeType, this.value);
      });
    }
    // Node size Listener
    document.getElementById('input-node-size').addEventListener('change', function() {
      state.set('options-node-size', this.value);
    });
  }

  nodeColorChange(value, name) {
    this.nodes.setColors(name.replace('options-node-color-',''), value);
  }

}

Controls.DEFAULTS = DEFAULTS;
export default Controls;
