// Constants for SequencerModule

export const pairedSequence = {};
export const sequence = 'TTGGGGGGACTGGGGCTCCCATTCGTTGCCTTTATAAATCCTTGCAAGCCAATTAACAGGTTGGTGAGGGGCTTGGGTGAAAAGGTGCTTAAGACTCCGT';
export const dbn = '...(((((.(...).)))))........(((((.....((..(.((((((..(((.((...)).)))..)))))).).)))))))...............';
export const validDNABases = ['A', 'C', 'G', 'T', 'N'];
