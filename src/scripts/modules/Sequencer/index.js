import {default as utilities} from '../Utilities';
import {default as state} from '../State';
import {default as StaticState} from '../StaticState';
import {default as Nodes} from '../Nodes';
import * as constants from './constants';

class SequencerModule extends StaticState {

  constructor(options) {
    super();
    console.info('Sequencer::Constructor', options);

    this.nodes = new Nodes();
    this.buildSequence = [];
    state.set('sequence', constants.sequence);
    state.set('dbn', constants.dbn);
    state.set('pairedSequence', constants.pairedSequence);
  }

  build() {
    console.group('Sequencer::build');
    let sequence = state.get('sequence');
    let dbn = state.get('dbn');
    state.buildSequence = [];

    if (sequence.length !== dbn.length) {
      console.error('Sequence & DBN don\'t match up.');
      return false;
    }

    let sequenceArray = Array.from(sequence);
    let dbnArray = Array.from(dbn);

    // Build Main Sequence Array
    for (let i = 0; i < sequenceArray.length; i++) {
      // Proofing for strange characters.
      let dbnFix = dbnArray[i];
      if (dbnFix !== '(' && dbnFix !== ')') {
        dbnFix = '.';
      }
      // Build the array.
      this.buildSequence.push({
        id: i,
        nucleotide: sequenceArray[i],
        dbn: dbnArray[i],
        association: null
      });
    }
    state.set('pairedSequence', this.buildSequence);

    // Make assoications.
    for (let i = 0; i < this.buildSequence.length; i++) {
      if (this.buildSequence[i].dbn === '(' && this.buildSequence[i].association === null) {
        // Find the matching set.
        let theAssoicationIndex = null;
        let bracketCount = 0;
        for (let a = i+1; a < this.buildSequence.length; a++) {
          if (this.buildSequence[a].dbn === ')') {
            if (bracketCount === 0) {
              theAssoicationIndex = a;
              this.buildSequence[a].association = i;
              break;
            } else {
              bracketCount--;
            }
          } else if (this.buildSequence[a].dbn === '(') {
            bracketCount++;
          } else {
            // Nothing to do, advance.
          }
        }
        this.buildSequence[i].association = theAssoicationIndex;
      } else {
        // console.info('Association SKIP @ %d:', i, this.buildSequence[i].dbn);
      }
    }

    console.info('pairedSequence', state.get('pairedSequence'));
    console.groupEnd();
  }

  setPositions() {
    let sequence = state.get('pairedSequence');

    let currentPosMark = {x: 20, y: 400};
    let associationPosAdjust = 50;
    let nodeDistance = 20;
    let assoicationCounter = 0;
    let assoicationDirection = 0; // 0 = null, 1 = increse, -1 = decrese

    for (const key in Object.keys(sequence)) {
      let keyInt = parseInt(key);
      // console.info('assoicationCounter', assoicationCounter);

      // Debug stopper
      // if (keyInt > 22) {
      //   return;
      // }

      let currentNode = sequence[key];
      let nodePreviousAssoicated = false;
      let nodeNextAssoicated = false;

      if (keyInt !== 0) {
        if (sequence[keyInt-1].association !== null) {
          nodePreviousAssoicated = true;
        }
      }
      if (keyInt !== (sequence.length - 1)) {
        if (sequence[keyInt+1].association !== null) {
          nodeNextAssoicated = true;
        }
      }

      if (currentNode.association && currentNode.association > keyInt) {
        // console.log('key(%s): if', keyInt + 1);
        assoicationCounter++;
        assoicationDirection = 1;
        // set position of assoicated node.
        this.nodes.setPos(currentNode.id, currentPosMark);
        let associationPos = {
          x: currentPosMark.x + associationPosAdjust,
          y: currentPosMark.y
        };
        this.nodes.setPos(currentNode.association, associationPos);

        // Set position for next node.
        currentPosMark.y = currentPosMark.y + nodeDistance;
        // currentPosMark.x = currentPosMark.x + nodeDistance;

      } else if (currentNode.association && currentNode.association <= keyInt) {
        // console.log('key(%s): elseif', keyInt + 1, nodePreviousAssoicated && nodeNextAssoicated);
        assoicationCounter--;
        assoicationDirection = -1;
        currentPosMark.y = currentPosMark.y - nodeDistance;
        // currentPosMark.x = currentPosMark.x + nodeDistance;
        if (nodePreviousAssoicated && nodeNextAssoicated) {
          // ???
          let alteredPos = {
            x: currentPosMark.x, //+ 10,
            y: currentPosMark.y
          }
          this.nodes.setPos(currentNode.id, alteredPos);
          currentPosMark.y = currentPosMark.y - nodeDistance;
        } else {
          // Known-known
          // currentPosMark.x = currentPosMark.x + nodeDistance;
        }
      } else {
        // console.log('key(%s): elsed', keyInt + 1, nodePreviousAssoicated && nodeNextAssoicated && (assoicationCounter > 0));
        // Set position for next node.
        //
        if (nodePreviousAssoicated && nodeNextAssoicated && (assoicationCounter > 0)) {
          // ???
          let alteredPos = {
            x: currentPosMark.x - 10,
            y: currentPosMark.y
          }
          currentPosMark.y = currentPosMark.y + nodeDistance;
          if (assoicationDirection === -1) {
            alteredPos = {
              x: currentPosMark.x + 10,
              y: currentPosMark.y - (2 * nodeDistance)
            }
            currentPosMark.y = currentPosMark.y - (2 * nodeDistance);
          }
          this.nodes.setPos(currentNode.id, alteredPos);
        } else {
          // Known-known
          this.nodes.setPos(currentNode.id, currentPosMark);
          currentPosMark.x = currentPosMark.x + nodeDistance;
        }
      }

      if (assoicationCounter < 1) {
        assoicationDirection = 0;
        assoicationCounter = 0;
      }

      // set pointer for next node.
    }
  }

  verifySequence() {
    // Check if Sequence & DBN are same length
    if (state.get('sequence').length !== state.get('dbn').length) {
      // Sequence & DBN don't match up.
      return false;
    } else if (utilities.countOccurances(state.get('dbn'), '(') !== utilities.countOccurances(state.get('dbn'), ')')) {
      // DBN doesn't enclose properly.
      return false;
    } else if (!utilities.countOccurancesNot(state.get('sequence'), constants.validDNABases)) {
      // Sequence has characters beyond accpted characters.
      return false;
    } else {
      return true;
    }
  }

}

export default SequencerModule;
