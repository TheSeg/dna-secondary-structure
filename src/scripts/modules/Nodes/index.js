import {default as state} from '../State';
import {default as utilities} from '../Utilities';

const DEFAULTS = {

}

class NodesModule {

  constructor(options) {
    console.info('Nodes::Constructor', options);

    state.watch('options-node-color-c', this.setColors);
    state.watch('options-node-color-g', this.setColors);
    state.watch('options-node-color-a', this.setColors);
    state.watch('options-node-color-t', this.setColors);
    state.watch('options-node-color-n', this.setColors);
    state.watch('options-node-size', this.setNodeSize);

  }

  createNode(nodeData) {
    let mainStage = state.get('mainStage');

    // Declare varaibles
    let initalPlacement = 'translate(' + (10 + (nodeData.id * 8)) + ',' + (10 + (nodeData.id * 4)) + ')';
    let backgroundColor = state.get('options-node-color-' + nodeData.nucleotide.toLowerCase());
    let forgroundColor = utilities.getContrastYIQ(backgroundColor);

    let group = mainStage.append('g')
      .attr('class', 'group-node group-node-' + nodeData.nucleotide)
      .attr('data-node-id', nodeData.id)
      .attr('data-node-association', nodeData.association)
      .attr('transform', initalPlacement)
      .call(d3.drag()
        .on('drag', this.eventDragged)
      )
      ;

    group.append('circle')
      .attr('class', 'node')
      .attr('r', 10)
      .attr('fill', backgroundColor)
      .data(nodeData)
      ;

    group.append('text')
      // .text(nodeData.nucleotide)
      // .text(nodeData.id)
      .text((nodeData.id + 1))
      .attr('class', 'node-text')
      .attr('fill', forgroundColor)
      .attr('text-anchor', 'middle')
      .attr('dy', 5)
      // .exit()
      ;
  }

  setPos(nodeID, posArray) {
    let placement = 'translate(' + posArray.x + ',' + posArray.y + ')';
    d3.select('[data-node-id=\'' + nodeID + '\']')
      .attr('transform', placement)
    ;
  }

  setColors(targetColor, targetType) {
    // Applies current setting colors to all nodes.
    targetType = targetType.replace('options-node-color-','').toUpperCase();
    let backgroundColor = targetColor;
    let forgroundColor = utilities.getContrastYIQ(backgroundColor);

    d3.selectAll('.group-node-' + targetType)
      .select('circle')
      .attr('fill', backgroundColor)
    ;
    d3.selectAll('.group-node-' + targetType)
      .select('text')
      .attr('fill', forgroundColor)
    ;
  }

  setNodeSize(targetValue) {
    d3.selectAll('.group-node')
      .select('circle')
      .attr('r', targetValue)
    ;
  }

  eventDragged() {
    let x = d3.event.x;
    let y = d3.event.y;

    d3.select(this).attr('transform', 'translate(' + x + ',' + y + ')');
  }
}

NodesModule.DEFAULTS = DEFAULTS;

export default NodesModule;
